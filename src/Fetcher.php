<?php

namespace Madhavim\MetatagsFetcher;

/**
 * Implementation of MetatagsFetcher.
 */
class Fetcher {
  // Parse feed URL to return feed item links.
  public function parseUrl($url) {
    $url = trim($url);
    $rss = simplexml_load_file($url);

    foreach ($rss->channel as $channel) {
      foreach ($channel->item as $item) {
        $items[] = $item->link;
      }
    }
    foreach ($items as $item) {
      $feedItems[] = $item->__toString();
    }
    return $feedItems;
  }

  // Fetch metatags from feed items URL.
  public function fetchMetatags ($url) {
    $curlInit = curl_init();

    curl_setopt($curlInit, CURLOPT_HEADER, 0);
    curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlInit, CURLOPT_URL, $url);
    curl_setopt($curlInit, CURLOPT_FOLLOWLOCATION, 1);

    $htmlDocument = curl_exec($curlInit);
    curl_close($curlInit);

    $doc = new \DOMDocument('1.0', 'UTF-8');
    $doc->loadHTML($htmlDocument);

    // fecth <title>.
    $tags['title'] = $doc->getElementsByTagName('title')->item(0)->nodeValue;

    // fetch og:tags and twitter:tags.
    foreach($doc->getElementsByTagName('meta') as $metatags ) {
      // if had property
      if ($metatags->getAttribute('property')) {

        $properties = $metatags->getAttribute('property');

        // here search only og:tags
        if (preg_match("/og:/i", $properties)) {
          $tags['og_tags'][$metatags->getAttribute('property')] = [
            'property' => $metatags->getAttribute('property'),
            'content' => $metatags->getAttribute('content')
          ];
        }
        // here search only twitter:tags
        if (preg_match("/twitter:/i", $properties)) {
          $tags['twitter_tags'][$metatags->getAttribute('property')] = [
            'property' => $metatags->getAttribute('property'),
            'content' => $metatags->getAttribute('content')
          ];
        }
      }
      // fetch <meta name="description" ... >.
      if( $metatags->getAttribute('name') == 'description' ) {
        $tags['description'] = $metatags->getAttribute('content');
      }
    }

    // render JSON
    return $tags;
  }
}